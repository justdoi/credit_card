import cv2
import numpy as np
import matplotlib.pyplot as plt
def cv_show(win,img):
    cv2.namedWindow(win,cv2.WINDOW_NORMAL)
    cv2.imshow(win,img)
    fx=cv2.waitKey(0)
    if fx==115:
        cv2.imwrite("result.jpg",img)
    cv2.destroyAllWindows()
def sortcout(cnt):
    x=cv2.boundingRect(cnt)[0]#比较外接矩形的左上角坐标大小即可从左到右排序
    return x
#处理模板图片
templa=cv2.imread("ocr_a_reference.png")
# print(templa.shape)
# print(templa[100:126,100:400].shape)
# print(templa)
#转换为灰度图
templat=cv2.cvtColor(templa,cv2.COLOR_BGR2GRAY)
#将图像转为二值化图像
ret,template=cv2.threshold(templat,5,255,cv2.THRESH_BINARY_INV)
# print(templa)
# cv_show("template",templa)
#找到每个数字的外轮廓
counter,hier=cv2.findContours(template,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
# cv2.drawContours(templa,counter,-1,(0,0,255),2)
# cv_show("templa",templa)
print(counter)
# print(len(counter))
#轮廓排序
counter.sort(key=sortcout,reverse=False)#对轮廓进行排序，从左到右，排序算法是sortcout，reverse表示是否倒转排序，默认为从小到大
# x,y,w,h=cv2.boundingRect(counter[4])
# cv_show("img",templat[y:y+h,x:x+w])
# print(x,y,w,h)
#将每个数字对应的模板放入字典
dis={}
for i,count in enumerate(counter):
    x,y,w,h=cv2.boundingRect(count)
    roi=template[y:y+h,x:x+w]
    roi=cv2.resize(roi,(60,80))
    dis[i]=roi
# print(dis[0].shape)
# cv_show("img",dis[2])

#处理目标文件
imags=cv2.imread("credit_card_05.png")
#转换为灰度图
imag=cv2.cvtColor(imags,cv2.COLOR_BGR2GRAY)
# #高斯滤波使图像更干净便于数字识别
# imag=cv2.GaussianBlur(imag,(3,3),0)
# cv_show("imag",imag)
kernel=np.ones((5,5),np.uint8)
#进行礼帽操作，突出原图周围高亮的部分
tophat=cv2.morphologyEx(imag,cv2.MORPH_TOPHAT,kernel)
cv_show("tophat",tophat)
#sobel算子找到边缘信息
sob1=cv2.Sobel(tophat,-1,1,0)
sob2=cv2.Sobel(tophat,-1,0,1)
sob=cv2.addWeighted(sob1,0.5,sob2,0.5,0)
cv_show("sob",sob)
#创建一个卷积核
kernel1=np.ones((3,11),np.uint8)
#进行闭运算，先膨胀后腐蚀，使数字连在一起
open1=cv2.morphologyEx(sob,cv2.MORPH_CLOSE,kernel1,iterations=2)
cv_show("open1",open1)
#二值化处理，用OTSU阈值处理方法方便计算阈值
ret,ima=cv2.threshold(open1,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv_show("ima",ima)
cnts,heri=cv2.findContours(ima,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
# cv2.drawContours(imags,cnts,-1,(0,0,255),2)
# cv_show("imags",imags)
cnts.sort(key=sortcout,reverse=False)#排序，从左到右
#每一段数字提取出来
roit=[]
for i in cnts:
    x,y,w,h=cv2.boundingRect(i)
    if y>0.45*(imags.shape[0]) and y<0.6*(imags.shape[0]):
        if w>0.1*(imags.shape[1]):#两个判断条件筛选出ROI区域
            roii=imag[y-3:y+h+3,x-3:x+w+3]
            cv_show("saa",roii)
            ret,roii=cv2.threshold(roii,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            k=np.ones((3,3),np.uint8)
            roii=cv2.morphologyEx(roii,cv2.MORPH_CLOSE,k)
            cv_show("sssss",roii)
            roit.append((roii,(x,y,w,h)))

# cv_show("img",roit[0])
# print(len(roit))
#将每段数字中的数字提取出并进行模板匹配
result=[]
for i,(gx,gy,gw,gh) in roit:
    cntt,herr=cv2.findContours(i,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cntt.sort(key=sortcout,reverse=False)
    for j in cntt:
        x,y,w,h=cv2.boundingRect(j)
        targetroi=i[y:y+h,x:x+w]#提取ROI区域
        cv_show("ss",targetroi)
        target=cv2.resize(targetroi,(60,80))#模板和样本尺寸保持一致
        score=[]
        for key in dis:
            res=cv2.matchTemplate(target,dis[key],cv2.TM_CCORR)
            v=cv2.minMaxLoc(res)[1]
            score.append(v)
        result.append(str(np.argmax(score)))#将结果放入列表
        #将识别出的数字写入图片，第一个参数是要写入的图片，第二个是写入的字符，第三个参数是写入字符的左下角坐标，第四个是字体，第五个是字体大小，第六个是颜色，第七个是粗细
        cv2.putText(imags,str(np.argmax(score)),(gx+x-10,gy+y-10),cv2.FONT_HERSHEY_PLAIN,2.5,(0,0,255),2)
    cv2.rectangle(imags, (gx-2, gy-2), (gx + gw+2, gy + gh+2), (255, 255, 0), 1)
# print(result)
cv_show("imags",imags)