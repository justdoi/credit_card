cmake_minimum_required(VERSION 3.16)
project(credit_card)

set(CMAKE_CXX_STANDARD 17)
set(OpenCV_DIR "/home/liyankuan/demo/win/opencv-3.4.7/build")
set(${OpenCV_INCLUDE_DIRS} "/home/liyankuan/demo/win/opencv-3.4.7/include/opencv2/")
set(${OpenCV_LIBS}  "/home/liyankuan/demo/win/opencv-3.4.7/build/lib/")

add_executable(credit_card main.cpp)
find_package(OpenCV 3.4.7 REQUIRED)
target_link_libraries(credit_card ${OpenCV_LIBS})