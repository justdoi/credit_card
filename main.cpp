#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
using namespace std;
using namespace cv;
void cv_show(const char *win, Mat &img)
{
    namedWindow(win,WINDOW_NORMAL);
    imshow(win,img);
    int fx= waitKey(0);
    if (fx==115)
    {
        imwrite("result.jpg",img);
    }
    destroyAllWindows();
}
void sort_cnt(vector<vector<Point>>&cnts)
{
	for (int i=0;i<cnts.size();i++)
	{
		for (int j=i;j<cnts.size();j++)
		{
			
			int x= boundingRect(cnts.at(j)).x;
			int t_x= boundingRect(cnts.at(i)).x;
			if (x<t_x)
			{
				vector<Point>cn(cnts.at(i));
				cnts[i]=cnts[j];
				cnts[j]=cn;
			}
		}
	}
}

void gettamplate(vector<Mat>&templa)
{
    Mat img=imread("../ocr_a_reference.png");
    Mat imag=img.clone();
//    cv_show("img",imag);
    cvtColor(imag,imag,COLOR_BGR2GRAY);
    threshold(imag,imag,5,255,THRESH_BINARY_INV);
//	cv_show("img",imag);
    vector<vector<Point>>cnts;
    vector<Vec4i>hier;
    findContours(imag,cnts,hier,RETR_EXTERNAL,CHAIN_APPROX_NONE);
	sort_cnt(cnts);
//	int a=0;
	for (auto & cnt : cnts)//auto关键字自动推断变量的数据类型
	{
		Rect tem_rec;
		tem_rec= boundingRect(cnt);
		Mat temp=imag(tem_rec);
		resize(temp,temp,Size(60,80),0,0);
		templa.push_back(temp);
//		cv_show("templa_a",templa[a]);
//		a++;
	}
//	drawContours(img,cnts,4,Scalar(0,0,255),2,8);
//	cv_show("imag",img);
}
void get_sample(Mat &src,vector<Mat>&sample,vector<Rect>&pos)
{
	Mat img=src.clone();
	int row=img.rows;
	int col=img.cols;
	cvtColor(img,img,COLOR_BGR2GRAY);
	Mat kernel1=Mat::ones(Size(9,9),CV_8UC1);
	Mat ima;
	morphologyEx(img,ima,MORPH_TOPHAT,kernel1,Point(-1,-1));
	bilateralFilter(ima,img,5,80,0);
	Mat gray=img.clone();
//	cv_show("gray",gray);
//	cv_show("img",img);
	Mat img1,img2;
	Sobel(img,img1,-1,1,0);
	Sobel(img,img2,-1,0,1);
	addWeighted(img1,0.5,img2,0.5,0,img);
	Mat kernel2=Mat::ones(Size(11,3),CV_8UC1);
	morphologyEx(img,img,MORPH_CLOSE,kernel2,Point(-1,-1),2);
//	cv_show("img",img);
	threshold(img,img,0,255,CV_THRESH_BINARY+CV_THRESH_OTSU);
//	cv_show("img",img);
	vector<vector<Point>>contour;
	findContours(img,contour,RETR_EXTERNAL,CHAIN_APPROX_NONE);
	sort_cnt(contour);
	vector<vector<Point>>re_cnt;
	for (auto &cn:contour)
	{
		Rect rec;
		rec=boundingRect(cn);
		if (rec.y>0.45*row&&rec.y<0.6*row&&rec.width>0.1*col)
		{
			rec.x-=2;
			rec.y-=2;
			rec.width+=4;
			rec.height+=4;
			sample.push_back(gray(rec));
			pos.push_back(rec);
		}
	}
//	drawContours(src,contour,-1,Scalar(0,0,255),2,8);
//	cv_show("src",src);
}
void match_temp(Mat &sam,vector<Mat>&temp,vector<string>&re)
{
	vector<vector<Point>>ds_cnt;
	threshold(sam,sam,0,255,CV_THRESH_BINARY+CV_THRESH_OTSU);
//	cv_show("sam",sam);
	findContours(sam,ds_cnt,RETR_EXTERNAL,CHAIN_APPROX_NONE);
	sort_cnt(ds_cnt);
	string s;
	for (auto &cn:ds_cnt)
	{
		Rect box;
		box=boundingRect(cn);
		Mat img=sam(box);
		resize(img,img,Size(60,80),0,0);
//		cv_show("img",img);
		Mat dst;
		double maxval=0,minval=0,reval;
		int index=0;
		Point minp(0,0);
		Point maxp(0,0);
		vector<double>val;
		for (int j=0;j<temp.size();j++)
		{
			matchTemplate(img,temp[j],dst,TM_CCORR);
			minMaxLoc(dst,&minval, &maxval,&minp,&maxp);
			cout<<maxval<<"   ";
			if (j==0)
			{
				reval=maxval;
			}
			else
			{
				if (maxval>reval)
				{
					reval=maxval;
					index=j;
				}
			}
		}
		cout<<endl;
		char ss= index + 48;
		s+=ss;
	}
	re.push_back(s);
}
int main()
{
	namedWindow("src",WINDOW_NORMAL);
	Mat src= imread("../credit_card_05.png");
    vector<Mat>templa;
    gettamplate(templa);
    vector<Mat>sample;
    vector<Rect>pos;
	get_sample(src,sample,pos);
//	for (auto &sam:sample)
//	{
//		cv_show("sam",sam);
//	}
	vector<string>result;
	for (auto &sa:sample)
	{
		match_temp(sa,templa,result);
	}
//	for (int i=0;i<result.size();i++)
//	{
//		cout<<result.at(i);
//	}
	for(int i=0;i<pos.size();i++)
	{
		rectangle(src,pos[i],Scalar(0,0,255),2,LINE_8);
		putText(src,result[i],Point(pos[i].x-3,pos[i].y-5),CV_FONT_HERSHEY_COMPLEX_SMALL,2,Scalar(0,0,255),2);
	}
	imshow("src",src);
	waitKey(0);
	destroyAllWindows();
    return 0;
}
